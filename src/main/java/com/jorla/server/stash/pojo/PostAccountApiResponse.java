package com.jorla.server.stash.pojo;

import java.util.Map;

public class PostAccountApiResponse {
	
	String authtoken;
	
	public PostAccountApiResponse(String authtoken) {
		this.authtoken = authtoken;
	}

	public String getAuthtoken() {
		return authtoken;
	}

	public void setAuthtoken(String authtoken) {
		this.authtoken = authtoken;
	}


}
