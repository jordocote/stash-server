package com.jorla.server.stash.pojo;

import java.util.List;
import java.util.Map;

public class PutStashApiResponse {

	List<RemoteIdPair> remoteIdMap;
	
	public PutStashApiResponse() {

	}

	public List<RemoteIdPair> getRemoteIdMap() {
		return remoteIdMap;
	}

	public void setRemoteIdMap(List<RemoteIdPair> remoteIdMap) {
		this.remoteIdMap = remoteIdMap;
	}

}
