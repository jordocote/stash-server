package com.jorla.server.stash.pojo;

import java.util.List;

import com.jorla.server.stash.mobile.FollowMobile;



public class GetUsernameApiRequest {

	private List<String> fbUsernameList;

	public List<String> getFbUsernameList() {
		return fbUsernameList;
	}

	public void setFbUsernameList(List<String> fbUsernameList) {
		this.fbUsernameList = fbUsernameList;
	}

}
