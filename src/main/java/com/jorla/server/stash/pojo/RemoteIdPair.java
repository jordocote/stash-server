package com.jorla.server.stash.pojo;

public class RemoteIdPair {
	Long mobileId;
	Long serverId;
	public RemoteIdPair() { }
	public RemoteIdPair(Long mobileId, Long serverId) {
		this.mobileId = mobileId;
		this.serverId = serverId;
	}
	public Long getMobileId() {
		return mobileId;
	}
	public void setMobileId(Long mobileId) {
		this.mobileId = mobileId;
	}
	public Long getServerId() {
		return serverId;
	}
	public void setServerId(Long serverId) {
		this.serverId = serverId;
	}
}	