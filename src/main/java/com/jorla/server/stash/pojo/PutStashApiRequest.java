package com.jorla.server.stash.pojo;

import java.util.List;

import com.jorla.server.stash.data.Stash;
import com.jorla.server.stash.mobile.StashMobile;



public class PutStashApiRequest {

	private List<StashMobile> stashList;

	public List<StashMobile> getStashList() {
		return stashList;
	}

	public void setStashList(List<StashMobile> stashList) {
		this.stashList = stashList;
	}

}
