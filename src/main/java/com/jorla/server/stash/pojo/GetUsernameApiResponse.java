package com.jorla.server.stash.pojo;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jorla.server.stash.mobile.UserMobile;

public class GetUsernameApiResponse {
	
	/*
	 * Mapping from facebook username to stash username
	 */
	List<UsernamePair> usernameMap;
	
	public GetUsernameApiResponse() {

	}

	public List<UsernamePair> getUsernameMap() {
		return usernameMap;
	}

	public void setUsernameMap(List<UsernamePair> usernameMap) {
		this.usernameMap = usernameMap;
	}

	@JsonIgnore
	public Map<String, UserMobile> getFacebookUsernameMap() {
		Map<String, UserMobile> map = new HashMap<String, UserMobile>();
		for (UsernamePair pair : this.usernameMap) {
			map.put(pair.getUsername(), pair.getUser());
		}
		return map;
	}
}
