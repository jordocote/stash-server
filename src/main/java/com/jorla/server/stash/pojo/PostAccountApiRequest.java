package com.jorla.server.stash.pojo;

import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.PathParam;

import com.jorla.server.stash.data.Stash;
import com.jorla.server.stash.mobile.StashMobile;
import com.jorla.server.stash.mobile.UserMobile;



public class PostAccountApiRequest {
	
	private String hashpass;
	private UserMobile user;
	private String fbtoken;

	public UserMobile getUser() {
		return user;
	}

	public void setUser(UserMobile user) {
		this.user = user;
	}

	public String getHashpass() {
		return hashpass;
	}

	public void setHashpass(String hashpass) {
		this.hashpass = hashpass;
	}

	public String getFbtoken() {
		return fbtoken;
	}

	public void setFbtoken(String fbtoken) {
		this.fbtoken = fbtoken;
	}

}
