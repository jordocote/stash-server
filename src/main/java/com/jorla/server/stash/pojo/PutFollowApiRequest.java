package com.jorla.server.stash.pojo;

import java.util.List;

import com.jorla.server.stash.mobile.FollowMobile;



public class PutFollowApiRequest {

	private List<FollowMobile> followList;

	public List<FollowMobile> getFollowList() {
		return followList;
	}

	public void setFollowList(List<FollowMobile> followList) {
		this.followList = followList;
	}

}
