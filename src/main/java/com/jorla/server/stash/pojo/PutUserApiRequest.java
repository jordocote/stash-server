package com.jorla.server.stash.pojo;

import java.util.List;

import com.jorla.server.stash.data.Stash;
import com.jorla.server.stash.mobile.StashMobile;
import com.jorla.server.stash.mobile.UserMobile;



public class PutUserApiRequest {

	private UserMobile user;

	public UserMobile getUser() {
		return user;
	}

	public void setUser(UserMobile user) {
		this.user = user;
	}

}
