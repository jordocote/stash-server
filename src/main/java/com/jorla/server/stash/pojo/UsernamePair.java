package com.jorla.server.stash.pojo;

import com.jorla.server.stash.mobile.UserMobile;

public class UsernamePair {
	String username;
	UserMobile user;
	
	public UsernamePair() { }
	public UsernamePair(String username, UserMobile userMobile) {
		this.username = username;
		this.user = userMobile;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public UserMobile getUser() {
		return user;
	}
	public void setUser(UserMobile userMobile) {
		this.user = userMobile;
	}

}	
