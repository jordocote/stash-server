package com.jorla.server.stash.pojo;

import java.util.Map;

public class GetAccountAuthTokenApiResponse {

	String authtoken;
	
	public GetAccountAuthTokenApiResponse(String authtoken) {
		this.authtoken = authtoken;
	}

	public String getAuthtoken() {
		return authtoken;
	}

	public void setAuthtoken(String authtoken) {
		this.authtoken = authtoken;
	}


}
