package com.jorla.server.stash.data;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Parameter;

import com.jorla.server.stash.mobile.UserMobile;
@Entity
@Table(name="users")
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1338931309551812229L;
	@Id
	@GeneratedValue(generator="AccountFK")
	  @GenericGenerator(name="AccountFK", strategy = "foreign", parameters={
	    @Parameter(name="property", value="account")
	  })
	private long id;
	
	@OneToOne(fetch = FetchType.LAZY, optional=false)
	@Cascade({org.hibernate.annotations.CascadeType.ALL})
	@PrimaryKeyJoinColumn // use Account table's primary key for joins
	private Account account;

	@Temporal(TemporalType.TIME)
	private Date createdOn;
	
	/*
	 * Used to determine if the client needs to sync this object
	 */
	@Temporal(TemporalType.TIME)
	private Date updatedOn;
	
	@Basic
	private boolean isDeleted;
	
	@Basic
	private String firstName;
	
	@Basic
	private String lastName;
	

	public User(UserMobile mobile) {
		setDeleted(		mobile.isDeleted());
		setCreatedOn(	new Date(mobile.getCreatedOn()));
		setFirstName(	mobile.getFirstName());
		setLastName(	mobile.getLastName());
		setUpdatedOn(	Calendar.getInstance().getTime());
	}
	
	public User() {
		
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	
}
