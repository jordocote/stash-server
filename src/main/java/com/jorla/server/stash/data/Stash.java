package com.jorla.server.stash.data;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.Session;

import com.jorla.server.stash.mobile.StashMobile;
@Entity
@Table(name="stash")
public class Stash implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5855475126790496509L;

	@Id
	@GeneratedValue
	private long id;

	@Temporal(TemporalType.TIME)
	private Date createdOn;

	/*
	 * Used to determine if the client needs to sync this object
	 */
	@Temporal(TemporalType.TIME)
	private Date updatedOn;
	
	@Basic
	private boolean isDeleted;

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
	@PrimaryKeyJoinColumn
	private Account createdBy;
		
	@Basic
	private String text;
	
	@Basic
	private double latitude;
	
	@Basic
	private double longitude;
	

	public Stash(StashMobile mobile) {
		DataStore dStore = DataStore.getInstance();
		Session session = dStore.createSession();
		if (mobile.getServerId() != 0) {
			setId(			mobile.getServerId());
		}
		setCreatedBy(	dStore.loadAccount(session, mobile.getCreatedByUsername()));
		setCreatedOn(	new Date(mobile.getCreatedOn()));
		setDeleted(		mobile.isDeleted());
		setText(		mobile.getText());
		setLatitude(	mobile.getLatitude());
		setLongitude(	mobile.getLongitude());
		setUpdatedOn(	Calendar.getInstance().getTime());
		dStore.closeSession(session);
	}

	public Stash() {
		
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Account getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Account createdBy) {
		this.createdBy = createdBy;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}
