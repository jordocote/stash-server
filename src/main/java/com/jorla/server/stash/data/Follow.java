package com.jorla.server.stash.data;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.Session;

import com.jorla.server.stash.mobile.FollowMobile;

@Entity
@Table(name="follow")
public class Follow implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5949010476229462826L;

	@Id
	@GeneratedValue
	private long id;

	@Temporal(TemporalType.TIME)
	private Date createdOn;

	/*
	 * Used to determine if the client needs to sync this object
	 */
	@Temporal(TemporalType.TIME)
	private Date updatedOn;
	
	@Basic
	private boolean isDeleted;

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
	@PrimaryKeyJoinColumn
	private Account follower;

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
	@PrimaryKeyJoinColumn
	private Account followed;
	

	public Follow(FollowMobile mobile) {
		DataStore dStore = DataStore.getInstance();
		Session session = dStore.createSession();
		setFollower(	dStore.loadAccount(session, mobile.getFollowerUsername()));
		setFollowed(	dStore.loadAccount(session, mobile.getFollowedUsername()));
		setCreatedOn(	new Date(mobile.getCreatedOn()));
		setDeleted(		mobile.isDeleted());
		setUpdatedOn(	Calendar.getInstance().getTime());
		dStore.closeSession(session);
	}

	public Follow() {
		
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Account getFollower() {
		return follower;
	}

	public void setFollower(Account follower) {
		this.follower = follower;
	}

	public Account getFollowed() {
		return followed;
	}

	public void setFollowed(Account followed) {
		this.followed = followed;
	}
}
