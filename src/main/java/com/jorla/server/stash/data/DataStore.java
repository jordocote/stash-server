package com.jorla.server.stash.data;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.sql.Update;




public class DataStore {
	private SessionFactory sessionFactory;
	private ServiceRegistry serviceRegistry;
	private static DataStore instance = null;
	
	public DataStore() {
	    Configuration configuration = new Configuration();
	    configuration
	    	.addAnnotatedClass(Stash.class)
	    	.addAnnotatedClass(User.class)
	    	.addAnnotatedClass(Account.class)
	    	.addAnnotatedClass(Follow.class)
	    	.addAnnotatedClass(com.jorla.server.stash.data.ApiSession.class)
	    	.configure();
	    serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);	
	}
	
	public synchronized static DataStore getInstance() {
		if (instance == null) {
			instance = new DataStore();
		}
		return instance;
	}

	public Session createSession() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		return session;
	}
	
	public void closeSession(Session session) {
		session.getTransaction().commit();
		session.close();
	}
	
	public void saveAccount(Session session, Account account, User user) {
		account.setUser(user);
		user.setAccount(account);
		session.saveOrUpdate(account);
		session.saveOrUpdate(user);
	}

	public <MODEL> MODEL load(long id, Class<MODEL> modelClass) {
		Session session = sessionFactory.openSession();
		MODEL model = (MODEL) session.get(modelClass, new Long(id));
		session.close();
		return model;
	}
	
	public <MODEL> MODEL load(Session session, String id, Class<MODEL> modelClass) {
		MODEL model = (MODEL) session.get(modelClass, id);
		return model;
	}

	
	public Account loadAccount(Session session, String username) {
		Account account = (Account) session
				.createQuery("SELECT a FROM Account AS a WHERE username = :UNAME")
				.setString("UNAME", username)
				.uniqueResult();
		return account;
	}
	
	public int invalidateSessions(Session session, long accountId) {
		String sql = "UPDATE Session SET active = :Active WHERE accountFk = :AccountId";
		Query query = session.createSQLQuery(sql)
			.addEntity(com.jorla.server.stash.data.ApiSession.class)
			.setParameter("Active", false)
			.setParameter("AccountId", accountId);
		int closed = query.executeUpdate();
		return closed;
	}
	

	public Account loadAccountByFbUsername(Session session, String fbUsername) {
		Account account = (Account) session
				.createQuery("SELECT a FROM Account AS a WHERE fbUsername = :FBUNAME")
				.setString("FBUNAME", fbUsername)
				.uniqueResult();
		return account;
	}
	
	public List<Follow> findRelevantFollows(Session session) {
		String sql = "SELECT * From follow";
		Query query = session.createSQLQuery(sql)
			.addEntity(Follow.class);
		List<Follow> follows = query.list();

		return follows;
	}
	
	public List<User> findRelevantUsers(Session session) {
		String sql = "SELECT * From users";
		Query query = session.createSQLQuery(sql)
			.addEntity(User.class);
		List<User> users = query.list();
		return users;
	}
	
	public List<Stash> findRadiusStash(Session session, double latitude, double longitude, double radius) {

		/*
    	// http://www.movable-type.co.uk/scripts/latlong-db.html
		String sql = "Select *, " +
			"acos(sin(:lat)*sin(radians(latitude)) + cos(:lat)*cos(radians(latitude))*cos(radians(longitude)-:lon)) * :R As D " +
			"From stash " + 
			"Where acos(sin(:lat)*sin(radians(latitude)) + cos(:lat)*cos(radians(latitude))*cos(radians(longitude)-:lon)) * :R < :rad";
		
		Query query = session.createSQLQuery(sql)
				.addEntity(Stash.class)
				.setParameter("lat", latitude)
				.setParameter("lon", longitude)
				.setParameter("rad", radius)
				.setParameter("R", 6371); // radius of earth in kilometers
		*/
		
		double earthRadius = 6371;
		double boxSpan = radius;
		double boxToEarthRatio = boxSpan / earthRadius;
		double degreesLatSpan = Math.toDegrees(boxToEarthRatio);
		// first-cut bounding box (in degrees)
		double maxLat = latitude + degreesLatSpan;
		double minLat = latitude - degreesLatSpan;
		// compensate for degrees longitude getting smaller with increasing latitude
		double degreesLongSpan = Math.toDegrees(boxToEarthRatio / Math.cos(Math.toRadians(latitude)));
		double maxLon = longitude + degreesLongSpan;
		double minLon = longitude - degreesLongSpan;
		
		String sql = "Select * from stash " + 
		        "Where latitude Between :minLat And :maxLat" +
		        " And longitude Between :minLon And :maxLon";
			
		Query query = session.createSQLQuery(sql)
				.addEntity(Stash.class)
				.setParameter("minLat", minLat)
				.setParameter("maxLat", maxLat)
				.setParameter("minLon", minLon)
				.setParameter("maxLon", maxLon);
		List<Stash> stashes = query.list();
		
		return stashes;
	}

	public void flush() {
		Session session = sessionFactory.openSession();
		session.flush();
		session.close();
	}

}
