package com.jorla.server.stash.data;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
@Table(name="account")
public class Account implements java.security.Principal {
	public enum Role {
		Viewer, User, Admin, SuperAdmin
    };
    
	@Id
	@GeneratedValue
	private long id;

	@NaturalId
	private String username;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "account") // account field of User table
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Basic
	private String email;
	
	@Temporal(TemporalType.TIME)
	private Date createdOn;
		
	@Basic
	private String hashpass;
		
	@Basic
	private String fbid;
	
	@Basic
	private String fbtoken;

	@Basic
	private String fbUsername;
	
	@Basic
	private String lastAuthToken;
	
	@Basic
    private Role role;
    
	public Account() {

	}
	
	public String getFbtoken() {
		return fbtoken;
	}

	public void setFbtoken(String fbtoken) {
		this.fbtoken = fbtoken;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getHashpass() {
		return hashpass;
	}

	public void setHashpass(String hashpass) {
		this.hashpass = hashpass;
	}

	public String getLastAuthToken() {
		return lastAuthToken;
	}

	public void setLastAuthToken(String lastAuthToken) {
		this.lastAuthToken = lastAuthToken;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	// Principal methods

	public String getName() {
		return username;
	}

	public void setName(String name) {
		this.username = name;
	}

	public String getFbUsername() {
		return fbUsername;
	}

	public void setFbUsername(String fbUsername) {
		this.fbUsername = fbUsername;
	}
	
	public String getFbid() {
		return fbid;
	}

	public void setFbid(String fbid) {
		this.fbid = fbid;
	}


}
