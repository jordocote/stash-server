package com.jorla.server.stash.exception;

import com.jorla.server.stash.exception.ApiException.ErrorCode;

public class ApiException extends Exception {

	public enum ErrorCode {
		UNKNOWN,
		RESOURCE_NOT_FOUND,
		RESOURCE_ALREADY_EXISTS,
		HASH_ALG_NOT_FOUND,
		AUTH_PROTO_VERSION_NOT_SUPPORTED,
				
	};
	
	private ErrorCode errorCode;

	public ApiException() {
		super();
		this.errorCode = ErrorCode.UNKNOWN;
	}
	
	public ApiException(ErrorCode errorCode, String message, Throwable cause) {
		super(message, cause);
		this.setErrorCode(errorCode);
	}

	public ApiException(ErrorCode errorCode, String message) {
		super(message);
		this.setErrorCode(errorCode);
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

}
