package com.jorla.server.stash.mobile;


import com.jorla.server.stash.data.Follow;

public class FollowMobile extends DataModel {
	private long id;
	
	private long createdOn;

	private String followerUsername;

	private String followedUsername;
	
	
	public FollowMobile() {
		
	}

	public FollowMobile(Follow server) {
		setFollowerUsername(	server.getFollower().getUsername());
		setFollowedUsername(	server.getFollowed().getUsername());
		setCreatedOn(	server.getCreatedOn().getTime());
		setDeleted(		server.isDeleted());
		setUpdatedOn(	server.getUpdatedOn().getTime());
	}

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}

	public String getFollowerUsername() {
		return followerUsername;
	}

	public void setFollowerUsername(String followerUsername) {
		this.followerUsername = followerUsername;
	}

	public String getFollowedUsername() {
		return followedUsername;
	}

	public void setFollowedUsername(String followedUsername) {
		this.followedUsername = followedUsername;
	}
	
}
