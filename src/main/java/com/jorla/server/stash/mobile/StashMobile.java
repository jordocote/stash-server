package com.jorla.server.stash.mobile;

import java.util.Calendar;
import java.util.Date;

import com.jorla.server.stash.data.DataStore;
import com.jorla.server.stash.data.Stash;
import com.jorla.server.stash.data.User;

/*
 * Stashed items
 */
public class StashMobile extends DataModel {

	private long id;
	
	private long serverId;
	
	private long createdOn;
	
	private String createdByUsername;
		
	private String text;
	
	private double latitude;
	
	private double longitude;

	public StashMobile() {
		
	}

	public StashMobile(Stash server) {
		setCreatedByUsername(	server.getCreatedBy().getUsername());
		setCreatedOn(	server.getCreatedOn().getTime());
		setDeleted(		server.isDeleted());
		setText(		server.getText());
		setLatitude(	server.getLatitude());
		setLongitude(	server.getLongitude());
		setServerId(	server.getId());
		setUpdatedOn(	server.getUpdatedOn().getTime());
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getCreatedByUsername() {
		return createdByUsername;
	}

	public void setCreatedByUsername(String createdByUsername) {
		this.createdByUsername = createdByUsername;
	}

	public long getServerId() {
		return serverId;
	}

	public void setServerId(long serverId) {
		this.serverId = serverId;
	}
	
}
