package com.jorla.server.stash.mobile;

import java.util.Calendar;
import java.util.Date;

import com.jorla.server.stash.data.User;


public class UserMobile extends DataModel {
	private long id;
	
	private long createdOn;

	private String username;
	
	private String firstName;
	
	private String lastName;

	private String email;
	
	private String fbid;
	
	private String fbUsername;
	
	public UserMobile() {
		
	}
	
	public UserMobile(User server) {
		setUpdatedOn(	server.getUpdatedOn().getTime());
		setDeleted(		server.isDeleted());
		setFirstName(	server.getFirstName());
		setLastName(	server.getLastName());
		setCreatedOn(	server.getCreatedOn().getTime());
		setFbid(		server.getAccount().getFbid());
		setFbUsername(	server.getAccount().getFbUsername());
		setEmail(		server.getAccount().getEmail());
		setUsername(	server.getAccount().getUsername());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String name) {
		this.username = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFbid() {
		return fbid;
	}

	public void setFbid(String fbid) {
		this.fbid = fbid;
	}

	public String getFbUsername() {
		return fbUsername;
	}

	public void setFbUsername(String fbUsername) {
		this.fbUsername = fbUsername;
	}

	
	
}
