package com.jorla.server.stash.mobile;

import java.util.Date;



public abstract class DataModel {
	
	private long updatedOn;
	
	private boolean isDeleted;

	public long getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(long updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public DataModel() {
		
	}
	
}
