package com.jorla.server.stash.security;

 
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Calendar;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.DatatypeConverter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
 
import com.jorla.server.stash.data.Account;
import com.jorla.server.stash.data.DataStore;
import com.jorla.server.stash.data.ApiSession;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;
 
/**
 * Filter all incoming requests, look for possible session information and use that
 * to create and load a SecurityContext to request.
 * @author "Animesh Kumar <animesh@strumsoft.com>"
 *
 */
@Provider    // register as jersey's provider
public class SecurityContextFilter implements ResourceFilter, ContainerRequestFilter, Filter {
      
    @Override
    public ContainerRequest filter(ContainerRequest request) {
        Account account = null;
        ApiSession apiSession = null;
        
        final String authorization = request.getRequestHeaders().getFirst("Authorization");
        System.out.println("AUTH:"+authorization);
        if (StringUtils.isEmpty(authorization)) {
        	request.setSecurityContext(new StashSecurityContext(null, null));
            return request;
        }
        
        if (!authorization.startsWith("Basic ")) {
        	return request;
        }
        
        final byte[] decodedBytes = Base64.decodeBase64(authorization.substring(6));
        if(decodedBytes == null || decodedBytes.length == 0){
            return request;
        }
        
        String authorizationDecoded = null;
		try {
			authorizationDecoded = new String(decodedBytes, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println("AUTH:"+authorizationDecoded);
        final String[] authorizationParts = authorizationDecoded.split(":", 2);
        if (authorizationParts.length != 2) {
        	
        }
        
        final String username = authorizationParts[0];
        System.out.println("AUTH:"+username);
        final String authtoken = authorizationParts[1];
        
        System.out.println("AUTH:"+authtoken);
        
    	DataStore dStore = DataStore.getInstance();
		Session session = dStore.createSession();
        if (!StringUtils.isEmpty(authtoken) && authtoken.length() == 16) {
        	System.out.println("Authtoken valid format");
	    	// Get Account
			Account candidateAccount = dStore.loadAccount(session, username);
			if (candidateAccount != null) {
				System.out.println("Got account");
				// Get & verify authtoken
				String serverAuthToken = candidateAccount.getLastAuthToken();
				System.out.println("AUTHSERVER:"+serverAuthToken);
				if (authtoken.equals(serverAuthToken)) {
					System.out.println("Authenticated");
					account = candidateAccount;
				}
			}
        }
        
//        if (account != null) {
//	        // Get session id from request header
//	        final String sessionId = request.getHeaderValue("session-id");
//	        if (!StringUtils.isEmpty(sessionId)) {
//	            // Load session object from repository
//	        	session = DataStore.getInstance().load(sessionId, Session.class);
//
//	            if (session != null) {
//	            	// check session matches account
//	            	if (session.getAccount().getId() == account.getId()) {
//	    	            
//	            	}
//	            }
//	        }
//        }
 
        if (account != null) {
	        apiSession = new ApiSession();
	        apiSession.setActive(true);
	        apiSession.setSecure(true);
	        apiSession.setAccount(account);
	        apiSession.setCreatedOn(Calendar.getInstance().getTime());
	        apiSession.setLastAccessedOn(Calendar.getInstance().getTime());
	        session.save(apiSession);
	        
	        // Set security context
	        request.setSecurityContext(new StashSecurityContext(apiSession, account));
        }
        dStore.closeSession(session);
        return request;
    }
 
    @Override
    public ContainerRequestFilter getRequestFilter() {
        return this;
    }
 
    @Override
    public ContainerResponseFilter getResponseFilter() {
        return null;
    }

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
}