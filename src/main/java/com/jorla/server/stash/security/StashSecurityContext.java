package com.jorla.server.stash.security;

import java.security.Principal;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.jorla.server.stash.data.Account;
import com.jorla.server.stash.data.Account.Role;
import com.jorla.server.stash.data.ApiSession;


public class StashSecurityContext implements SecurityContext {
    private final Account user;
    private final ApiSession session;
 
    public StashSecurityContext(ApiSession session, Account user) {
        this.session = session;
        this.user = user;
    }
    
	@Override
	public String getAuthenticationScheme() {
		return SecurityContext.BASIC_AUTH;
	}

	@Override
	public Principal getUserPrincipal() {
		return user;
	}

	@Override
	public boolean isSecure() {
		return (null != session) ? session.isSecure() : false;
	}

	@Override
	public boolean isUserInRole(String role) {
        if (null == session || !session.isActive()) {
            // Forbidden
            Response denied = Response.status(Response.Status.FORBIDDEN).entity("Permission Denied").build();
            throw new WebApplicationException(denied);
        }
 
        try {
            // this user has this role?
            return user.getRole().equals(Account.Role.valueOf(role));
        } catch (Exception e) {
        }
         
        return false;
	}

}
