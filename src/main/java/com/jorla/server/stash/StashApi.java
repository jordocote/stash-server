
package com.jorla.server.stash;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;

import com.jorla.server.stash.data.Account;
import com.jorla.server.stash.data.DataStore;
import com.jorla.server.stash.data.Follow;
import com.jorla.server.stash.data.ApiSession;
import com.jorla.server.stash.data.Stash;
import com.jorla.server.stash.data.User;
import com.jorla.server.stash.mobile.FollowMobile;
import com.jorla.server.stash.mobile.StashMobile;
import com.jorla.server.stash.mobile.UserMobile;
import com.jorla.server.stash.pojo.GetAccountAuthTokenApiResponse;
import com.jorla.server.stash.pojo.GetUsernameApiRequest;
import com.jorla.server.stash.pojo.GetUsernameApiResponse;
import com.jorla.server.stash.pojo.PostAccountApiRequest;
import com.jorla.server.stash.pojo.PostAccountApiResponse;
import com.jorla.server.stash.pojo.PutFollowApiRequest;
import com.jorla.server.stash.pojo.PutStashApiRequest;
import com.jorla.server.stash.pojo.PutStashApiResponse;
import com.jorla.server.stash.pojo.PutUserApiRequest;
import com.jorla.server.stash.pojo.RemoteIdPair;
import com.jorla.server.stash.pojo.UsernamePair;
import com.jorla.server.stash.security.StashSecurityContext;
import com.jorla.server.stash.util.StashUtil;

@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StashApi {
	
    @GET
    @Path("hi")
    public String hello() {
    	return "hi!";
    }
    
    /*
     * Create a new account using username and hashpass
     * Returns an authtoken
     * Creates a session so API calls are enabled
     */
    @POST
    @Path("account/{username:[a-zA-Z][a-zA-Z0-9%\\.]+}")
    @PermitAll
	public PostAccountApiResponse postAccount(
			@PathParam("username") String username,
			PostAccountApiRequest request
			) {
    	try {
			username = URLDecoder.decode(username, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).build());
		}
    	
    	String hashpass = request.getHashpass();
    	UserMobile userMobile = request.getUser();
    	String fbtoken = request.getFbtoken();
    	DataStore dStore = DataStore.getInstance();
    	Session session = dStore.createSession();
    	Account account = dStore.loadAccount(session, username);
    	if (account != null) {
    		// cannot create, account already exists
    		dStore.closeSession(session);
    		throw new WebApplicationException(Response.status(Status.CONFLICT).build());
    	}
    	
    	if (StringUtils.isNotEmpty(userMobile.getFbid()) && StringUtils.isNotEmpty(fbtoken)) {
    		// TODO check with facebook
    	} else if (StringUtils.isNotEmpty(hashpass) && hashpass.length() == 64) {
    		// ok
    	} else {
    		// need some kind of credentials
    		dStore.closeSession(session);
    		throw new WebApplicationException(Response.status(Status.BAD_REQUEST).build());
    	}
    	
		// create a random 256-bit auth token
		String authToken = StashUtil.createAuthToken();
    	
		// Create the User
		User userServer = new User(userMobile);
		
		if (!username.equals(userMobile.getUsername())) {
			dStore.closeSession(session);
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).build());
		}
		
		// Create the account
    	account = new Account();
		account.setUsername(username);
		account.setHashpass(hashpass);
		account.setFbtoken(fbtoken);
		account.setFbUsername(userMobile.getFbUsername());
		account.setFbid(userMobile.getFbid());
		account.setEmail(userMobile.getEmail());
		account.setLastAuthToken(authToken);
		account.setRole(Account.Role.User);
		account.setCreatedOn(Calendar.getInstance().getTime());
		dStore.saveAccount(session, account, userServer);
		
		// Create session
		ApiSession apiSession = ApiSession.createNew(account);
		session.save(apiSession);
		
		session.flush();
		dStore.closeSession(session);
		return new PostAccountApiResponse(authToken);
    }
    
    @GET
    @Path("account/{username:[a-zA-Z][a-zA-Z0-9%\\.]+}/auth")
    @RolesAllowed({"User"})
	public boolean verifyAuthToken(
			@PathParam("username") String username
			) {
    	// SecurityFilter has already authorized this user
    	return true;
    }
    
//    /*
//     * Using a stash authtoken, create a new session for this user
//     * Creates a session so API calls are enabled
//     */
//	@GET
//	@Path("account/{username:[a-zA-Z][a-zA-Z0-9]+}/auth")
//	public long getSession(
//			@PathParam("username") String username,
//			@QueryParam("authtoken") String authToken
//			) {
//    	if (StringUtils.isEmpty(authToken) || authToken.length() != 64) {
//			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).build());
//		}
//
//    	// Get Account
//    	DataStore dStore = DataStore.getInstance();
//		Account account = dStore.loadAccount(username);
//		if (account == null) {
//			// no account
//			throw new WebApplicationException(Response.status(Status.UNAUTHORIZED).build());
//		}
//		
//		// Get authtoken
//		String serverAuthToken = account.getLastAuthToken();
//		if (StringUtils.isEmpty(serverAuthToken)) {
//			// account has no created token
//			throw new WebApplicationException(Response.status(Status.UNAUTHORIZED).build());
//		}
//		
//		// Verify authtoken
//		if (!serverAuthToken.equals(authToken)) {
//			// token mismatch
//			throw new WebApplicationException(Response.status(Status.UNAUTHORIZED).build());
//		}
//
//		// Invalidate old sessions
//		dStore.invalidateSessions(account.getId());
//		
//		// Create new session
//		Session session = Session.createNew(account);		
//		dStore.save(session);
//		dStore.flush();
//		return session.getId();
//	}
	
	/*
	 * Request for a stash authtoken
	 * Requires username and hashpass used in account creation
	 * Creates a session so API calls are enabled
	 */
	@GET
	@Path("account/{username:[a-zA-Z][a-zA-Z0-9%\\.]+}/token")
	@PermitAll
	public GetAccountAuthTokenApiResponse getAccountAuthToken(
			@PathParam("username") String username,
			@QueryParam("hashpass") String hashpass,
			@QueryParam("fbid") String fbid,
			@QueryParam("fbtoken") String fbtoken

			) {
		DataStore dStore = DataStore.getInstance();
		Session session = dStore.createSession();
		Account account;
		if (StringUtils.isNotEmpty(fbid) && StringUtils.isNotEmpty(fbtoken)) {
			account = loadAccount(dStore, session, username);
			if (!fbid.equals(account.getFbid())) {
				// wrong fbid
				dStore.closeSession(session);
				throw new WebApplicationException(Response.status(Status.FORBIDDEN).build());
			}
			//TODO: check with facebook api
			// just allow for now
		} else if (StringUtils.isNotEmpty(hashpass) || hashpass.length() == 64) {
			account = loadAccount(dStore, session, username);
			if (!account.getHashpass().equals(hashpass)) {
				// wrong password
				dStore.closeSession(session);
				throw new WebApplicationException(Response.status(Status.FORBIDDEN).build());
			}			
		} else {
			dStore.closeSession(session);
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).build());
		}
		
		
		
		dStore.invalidateSessions(session, account.getId());
		
		ApiSession apiSession = ApiSession.createNew(account);
		session.saveOrUpdate(apiSession);
		
		// create a random 256-bit auth token
		String authToken = StashUtil.createAuthToken();
		// save the auth token
		account.setLastAuthToken(authToken);
		session.saveOrUpdate(account);
		dStore.closeSession(session);
		return new GetAccountAuthTokenApiResponse(authToken);
	}
	
	private Account loadAccount(DataStore dStore, Session session, String username) {
		Account account = dStore.loadAccount(session, username);
		if (account == null) {
			// cannot login with no account
			dStore.closeSession(session);
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).build());
		}
		return account;
	}
    
    /*
     * Push new or updated stashes to the server
     * Allow only those createdBy the authenticated user
     */
    @POST
    @Path("sync/stash")
    @RolesAllowed({"User"})
    public Response postStash(
    		@Context SecurityContext context,
    		PutStashApiRequest stashRequest) {
    	if (stashRequest == null) {
    		throw new WebApplicationException(666);
    	}
    	List<StashMobile> stashList = stashRequest.getStashList();
    	
    	if (stashList == null) {
    		throw new WebApplicationException(666);
    	}

    	final String authenticatedUsername = context.getUserPrincipal().getName();
    	PutStashApiResponse response = new PutStashApiResponse();
    	List<RemoteIdPair> remoteIdMap = new ArrayList<RemoteIdPair>();
    	response.setRemoteIdMap(remoteIdMap);
    	DataStore dStore = DataStore.getInstance();
    	Session session = dStore.createSession();
    	for (StashMobile mobileStash : stashList) {
    		if (!mobileStash.getCreatedByUsername().equals(authenticatedUsername)) {
    			continue;
    		}
    		Stash serverStash = new Stash(mobileStash);
    		serverStash.setUpdatedOn(Calendar.getInstance().getTime());
    		session.saveOrUpdate(serverStash);
    		session.flush();
    		if (mobileStash.getServerId() != serverStash.getId()) {
    			remoteIdMap.add(new RemoteIdPair(
    					mobileStash.getId(), serverStash.getId()));
    		}
    	}
    	dStore.closeSession(session);
		return Response.status(201).entity(response).build();
    }

    /*
     * Get all stashes accessible to user within a certain radius
     * which have updatedOn more recent than user's last sync
     */
    @GET
    @Path("sync/stash")
    @RolesAllowed({"User"})
    public List<StashMobile> syncStash(
    		@QueryParam("lat") double latitude,
    		@QueryParam("long") double longitude
    		) {
    	DataStore dStore = DataStore.getInstance();
    	Session session = dStore.createSession();
    	double radius = 50.0; // kilometers
    	List<Stash> stashes = dStore.findRadiusStash(session, latitude, longitude, radius);
    	List<StashMobile> stashMobiles = new ArrayList<StashMobile>();
    	for (Stash stash : stashes) {
    		stashMobiles.add(new StashMobile(stash));
    	}
    	dStore.closeSession(session);
		return stashMobiles;
    }

    /*
     * Get a specific stash based on server id
     */
    @GET
    @Path("stash/{id:[0-9]+}")
    @RolesAllowed({"User"})
    public StashMobile getStash(
            @PathParam("id") long id
            ) {
    	Stash serverStash = DataStore.getInstance().load(id, Stash.class);
    	StashMobile mobileStash = new StashMobile(serverStash);
    	return mobileStash;
    	
    }    
    
    @GET
    @Path("sync/user")
    @RolesAllowed({"User"})
    public List<UserMobile> syncUser() {
    	DataStore dStore = DataStore.getInstance();
    	Session session = dStore.createSession();
    	List<User> users = dStore.findRelevantUsers(session);
    	List<UserMobile> usersMobile = new ArrayList<UserMobile>();
    	for (User user : users) {
    		UserMobile userMobile = new UserMobile(user);
    		usersMobile.add(userMobile);
    	}
    	dStore.closeSession(session);
    	return usersMobile;
    }

    @PUT
    @Path("user/{username:[a-zA-Z][a-zA-Z0-9%\\.]+}")
    @RolesAllowed({"User"})
	public boolean putUser(
			@PathParam("username") String username,
			PutUserApiRequest putUserApiRequest
			) {
    	User user = new User(putUserApiRequest.getUser());
    	
    	DataStore dStore = DataStore.getInstance();
    	Session session = dStore.createSession();
		Account account = dStore.loadAccount(session, username);
		if (account == null) {
			System.out.println("account not found");
			dStore.closeSession(session);
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).build());
		}

		// Let mobile version clobber the server version
		// Use server version's id
		User serverUser = account.getUser();
		user.setId(serverUser.getId());
		user.setUpdatedOn(Calendar.getInstance().getTime());

    	dStore.saveAccount(session, account, user);
    	dStore.closeSession(session);
    	return true;
    }
    

    @PUT
    @Path("sync/follow")
    @RolesAllowed({"User"})
	public boolean putFollows(
			@Context SecurityContext context,
			PutFollowApiRequest followRequest) {
    	if (followRequest == null) {
    		throw new WebApplicationException(666);
    	}
    	List<FollowMobile> followList = followRequest.getFollowList();
    	
    	if (followList == null) {
    		throw new WebApplicationException(666);
    	}

    	final String authenticatedUsername = context.getUserPrincipal().getName();

    	DataStore dStore = DataStore.getInstance();
    	Session session = dStore.createSession();
    	for (FollowMobile mobileFollow : followList) {
    		// make sure that this user is the follower
    		if (!mobileFollow.getFollowerUsername().equals(authenticatedUsername)) {
    			continue;
    		}
    		Follow serverFollow = new Follow(mobileFollow);
    		serverFollow.setUpdatedOn(Calendar.getInstance().getTime());
    		session.saveOrUpdate(serverFollow);
    		session.flush();
    		dStore.closeSession(session);
    	}
    	
		return true;

    }

    @GET
    @Path("sync/follow")
    @RolesAllowed({"User"})
	public List<FollowMobile> syncFollows() {
    	DataStore dStore = DataStore.getInstance();
    	Session session = dStore.createSession();
    	List<Follow> follows = dStore.findRelevantFollows(session);
    	List<FollowMobile> followMobiles = new ArrayList<FollowMobile>();
    	for (Follow follow : follows) {
    		followMobiles.add(new FollowMobile(follow));
    	}
    	dStore.closeSession(session);
		return followMobiles;
    }

    @GET
    @Path("username/facebook")
    @RolesAllowed({"User"})
	public GetUsernameApiResponse getUsername(
			@QueryParam("fbUsernameList") List<String> fbUsernameList
			) {
    	List<UsernamePair> usernameMap = new ArrayList<UsernamePair>();
    	DataStore dStore = DataStore.getInstance();
    	Session session = dStore.createSession();
    	for (String fbUsername : fbUsernameList) {
			Account account = dStore.loadAccountByFbUsername(session, fbUsername);
			if (account == null) {
				System.out.println("account not found");
				dStore.closeSession(session);
				throw new WebApplicationException(Response.status(Status.BAD_REQUEST).build());
			}
			UserMobile userMobile = new UserMobile(account.getUser());
			usernameMap.add(new UsernamePair(fbUsername, userMobile));
    	}
    	GetUsernameApiResponse response = new GetUsernameApiResponse();
    	response.setUsernameMap(usernameMap);
    	dStore.closeSession(session);
		return response;
    }
    
}
