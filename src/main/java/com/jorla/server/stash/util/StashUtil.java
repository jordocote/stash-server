 package com.jorla.server.stash.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.RandomStringUtils;

import com.jorla.server.stash.data.DataStore;
import com.jorla.server.stash.exception.ApiException;
import com.jorla.server.stash.exception.ApiException.ErrorCode;

public class StashUtil {

	public static final String HASH_SALT_DOMAIN = "com.jorla.stash";
	public static final int AUTH_PROTO_V1 = 1;
	private StashUtil() {
		
	}
	public static String createAuthToken() {
		return RandomStringUtils.randomAlphanumeric(16);
	}
	


}
